<?php

namespace App\Controller\Web;

use App\Entity\Entity1;
use App\Form\Entity1Type;
use App\Repository\Entity1Repository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/entity1")
 */
class Entity1Controller extends AbstractController
{
    /**
     * @Route("/", name="entity1_index", methods={"GET"})
     */
    public function index(Entity1Repository $entity1Repository): Response
    {
        return $this->render('entity1/index.html.twig', [
            'entity1s' => $entity1Repository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="entity1_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $entity1 = new Entity1();
        $form = $this->createForm(Entity1Type::class, $entity1);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity1);
            $entityManager->flush();

            return $this->redirectToRoute('entity1_index');
        }

        return $this->render('entity1/new.html.twig', [
            'entity1' => $entity1,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="entity1_show", methods={"GET"})
     */
    public function show(Entity1 $entity1): Response
    {
        return $this->render('entity1/show.html.twig', [
            'entity1' => $entity1,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="entity1_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Entity1 $entity1): Response
    {
        $form = $this->createForm(Entity1Type::class, $entity1);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('entity1_index');
        }

        return $this->render('entity1/edit.html.twig', [
            'entity1' => $entity1,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="entity1_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Entity1 $entity1): Response
    {
        if ($this->isCsrfTokenValid('delete'.$entity1->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entity1);
            $entityManager->flush();
        }

        return $this->redirectToRoute('entity1_index');
    }
}

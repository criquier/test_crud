<?php

namespace App\Controller\Web;

use App\Entity\Entity2;
use App\Form\Entity2Type;
use App\Repository\Entity2Repository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/entity2")
 */
class Entity2Controller extends AbstractController
{
    /**
     * @Route("/", name="entity2_index", methods={"GET"})
     */
    public function index(Entity2Repository $entity2Repository): Response
    {
        return $this->render('entity2/index.html.twig', [
            'entity2s' => $entity2Repository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="entity2_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $entity2 = new Entity2();
        $form = $this->createForm(Entity2Type::class, $entity2);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity2);
            $entityManager->flush();

            return $this->redirectToRoute('entity2_index');
        }

        return $this->render('entity2/new.html.twig', [
            'entity2' => $entity2,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="entity2_show", methods={"GET"})
     */
    public function show(Entity2 $entity2): Response
    {
        return $this->render('entity2/show.html.twig', [
            'entity2' => $entity2,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="entity2_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Entity2 $entity2): Response
    {
        $form = $this->createForm(Entity2Type::class, $entity2);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('entity2_index');
        }

        return $this->render('entity2/edit.html.twig', [
            'entity2' => $entity2,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="entity2_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Entity2 $entity2): Response
    {
        if ($this->isCsrfTokenValid('delete'.$entity2->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entity2);
            $entityManager->flush();
        }

        return $this->redirectToRoute('entity2_index');
    }
}

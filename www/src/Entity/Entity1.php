<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\Entity1Repository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=Entity1Repository::class)
 */
class Entity1
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="Entity2", mappedBy="entity1", cascade={"PERSIST", "REMOVE"})
     */
    private $entities2;

    public function __construct()
    {
        $this->entities2 = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return ArrayCollection<Entity1>
     */
    public function getEntities2()
    {
        return $this->entities2;
    }

    public function clearEntities2()
    {
        $this->setEntities2(new ArrayCollection());
    }

    /**
     * @param ArrayCollection<Entity1> $entities2;
     */
    public function setEntities2(ArrayCollection $entities2)
    {
        $this->entities2 = $entities2;
    }

    /**
     * @param Entity2 $entity2
     */
    public function addEntity2(Entity2 $entity2)
    {
        $this->entities2->add($entity2);
    }

    /**
     * @param Entity2 $entity2
     */
    public function removeEntity2(Entity2 $entity2)
    {
        $entities2 = $this->entities2->toArray();
        foreach($entities2 as $index => $ent2) {
            if($entity2->equals($ent2)) {
                $this->entities2->remove($index);
                break;
            }
        }
    }

    /**
     * @param Entity1 $entity1
     * @return bool
     */
    public function equals(Entity1 $entity1)
    {
        return $this->id === $entity1->getId();
    }

    public function toJson()
    {
        return json_encode($this->toArray(true));
    }

    public function toArray($full=true)
    {
        $array = [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
        ];
        if ($full) {
            $array['entities2'] = [];
            foreach ($this->entities2 as $entity2) {
                $array['entities2'][] = $entity2->toArray();
            }
        }
        return $array;
    }
}

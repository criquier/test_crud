<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\Entity2Repository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=Entity2Repository::class)
 */
class Entity2
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var Entity1
     * @ORM\ManyToOne(targetEntity="Entity1", inversedBy="entities2")
     */
    private $entity1;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Entity1
     */
    public function getEntity1(): Entity1
    {
        return $this->entity1;
    }

    /**
     * @param Entity1 $entity1
     */
    public function setEntity1(Entity1 $entity1)
    {
        $this->entity1 = $entity1;
    }

    /**
     * @param Entity2 $entity2
     * @return bool
     */
    public function equals(Entity2 $entity2)
    {
        return $this->id === $entity2->getId();
    }

    public function toJson()
    {
        return json_encode($this->toArray(true));
    }

    public function toArray($full=true)
    {
        $array =[
            "id" => $this->id,
            "name"=> $this->name,
            "description" =>$this->description,
        ];
        if($full) {
            $array['entity1'] = $this->entity1->toArray();
        }
        return $array;
    }
}

# Installation
###Pré-requis
Installer le binaire symfony

Installer composer

### Etapes
Clone du projet

```git clone https://bitbucket.org/criquier/test_crud```

### Installation des vendors
 ```composer install```

Création d'un .env.local pour y intégrer l'url d'une bdd mysql

Création de la bdd

```symfony doctrine:database:create```

Création des tables

```symfony doctrine:migration:migrate```

Lancement du serveur symfony

```Symfony serve -d```

# Accès Web
Il y a 2 urls pour l'édition en twig des 2 entités

```127.0.0.1:8000/entity1```

```127.0.0.1:8000/entity2```

# Accès API
La documentation pour les urls de l'API disponible sur

```127.0.0.1/api```